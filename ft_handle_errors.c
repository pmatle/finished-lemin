/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_handle_errors.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 14:06:14 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 12:00:05 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int			ft_check_error(t_data data, t_start_end se, int ants)
{
	if (ants == 0)
	{
		ft_putendl_fd(CRED"Error: There are no ants exiting..."RSET, 2);
		free_memory(data);
		return (0);
	}
	if (data.head == NULL || data.node == NULL)
	{
		ft_putendl_fd(CRED"Error: Not enough information"RSET, 2);
		return (0);
	}
	if (se.start == -1 || se.end == -1)
	{
		ft_putendl_fd(CRED"Error: start or end node not specified"RSET, 2);
		free_memory(data);
		return (0);
	}
	return (1);
}
