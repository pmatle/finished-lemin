/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialization.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 09:15:32 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 10:21:39 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_istruct	initialization(t_graph **graph, t_infor *head)
{
	t_istruct	init;
	int			count;

	init.node1 = NULL;
	init.node2 = NULL;
	init.temp = head;
	count = count_nodes(head);
	*graph = create_graph(count);
	return (init);
}
