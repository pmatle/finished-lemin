# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmatle <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/09/07 10:55:12 by pmatle            #+#    #+#              #
#    Updated: 2017/11/14 12:11:04 by pmatle           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lemin

SRC = add_link.c add_node.c add_to_moving.c add_to_stack.c cal_distance.c\
	  count_node.c create_graph.c find_paths.c free_memory.c get_moving_node.c\
	  get_node.c initialization.c inspect_line.c list_manage.c make_path.c\
	  merge_sort_list.c move_ants.c read_map.c run_algorithm.c show_movement.c\
	  ft_handle_errors.c free_2d.c

OBJ = add_link.o add_node.o add_to_moving.o add_to_stack.o cal_distance.o\
	  count_node.o create_graph.o find_paths.o free_memory.o get_moving_node.o\
	  get_node.o initialization.o inspect_line.o list_manage.o make_path.o\
	  merge_sort_list.o move_ants.o read_map.o run_algorithm.o show_movement.o\
	  ft_handle_errors.o free_2d.o

FLAGS = -Wall -Wextra -Werror 

LIB = -L libft/ -lft

all: $(NAME)

$(NAME):
	make -C libft/ all
	gcc -o $(NAME) $(SRC) $(FLAGS) $(LIB) Get_next_line/get_next_line.c -g -g3	
clean:
	make -C libft/ clean
	/bin/rm -f $(OBJ)

fclean: clean
	/bin/rm -f $(NAME)
	make -C libft/ fclean

re:	fclean all
