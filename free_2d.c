/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_2d.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 10:09:20 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 10:10:18 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		free_2d(char **list)
{
	int		x;

	x = 0;
	while (list[x])
	{
		free(list[x]);
		x++;
	}
	free(list);
	return (1);
}
