/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_distance.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 16:57:02 by pmatle            #+#    #+#             */
/*   Updated: 2017/09/07 10:29:53 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

float		cal_sqrt(int answer)
{
	float	root;
	int		x;

	x = 0;
	root = 1;
	while (x < answer)
	{
		root = 0.5 * (root + answer / root);
		x++;
	}
	return (root);
}

int			cal_distance(int x1, int y1, int x2, int y2)
{
	float	sq;
	float	answer;
	int		sqr;

	answer = ((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1));
	sq = cal_sqrt(answer);
	sqr = (sq < 0) ? sq - 0.5 : sq + 0.5;
	return (sqr);
}
