/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_memory.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 08:31:00 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 11:09:48 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		free_memory(t_data data)
{
	t_con	*temp;
	t_infor	*temp1;

	while (data.head)
	{
		temp1 = data.head;
		data.head = data.head->next;
		free(temp1);
	}
	while (data.node)
	{
		temp = data.node;
		data.node = data.node->next;
		free(temp);
	}
	return (1);
}
