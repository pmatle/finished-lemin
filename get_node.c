/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_node.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 07:23:39 by pmatle            #+#    #+#             */
/*   Updated: 2017/09/07 07:24:42 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_infor		*get_node(t_infor *head, char *str)
{
	while (head != NULL)
	{
		if (ft_strcmp(head->room, str) == 0)
		{
			break ;
		}
		head = head->next;
	}
	return (head);
}
