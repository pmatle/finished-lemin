/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inspect_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 07:20:29 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 10:14:17 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	inspect_line(t_infor **head, t_con **node, char *line)
{
	char		**list;
	int			x;
	static int	pos = 0;

	x = 0;
	if (ft_strcmp(line, "##start") == 0)
		pos = 1;
	else if (ft_strcmp(line, "##end") == 0)
		pos = 2;
	else
	{
		list = ft_strsplit(line, ' ');
		while (list[x])
			x++;
		if (x == 3 && line[0] != '#' && line[0] != 'L')
		{
			add_node(&(*head), list, pos);
			pos = 0;
		}
		if (x == 1 && line[0] != '#' && line[0] != 'L')
		{
			add_link(&(*node), list[0]);
		}
		free_2d(list);
	}
}
