/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_link.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 07:09:06 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 10:13:42 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	add_link(t_con **head, char *str)
{
	t_con	*node;
	t_con	*temp;

	node = (t_con*)malloc(sizeof(*node));
	if (!node)
		return ;
	node->str = ft_strdup(str);
	node->next = NULL;
	if (*head == NULL)
	{
		(*head) = node;
	}
	else
	{
		temp = *head;
		while (temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = node;
	}
}
