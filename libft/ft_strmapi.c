/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 03:01:20 by pmatle            #+#    #+#             */
/*   Updated: 2017/06/11 17:26:09 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*str;
	int		x;
	int		y;

	if (s == NULL)
		return (NULL);
	x = ft_strlen((char*)s);
	y = 0;
	str = (char*)malloc(sizeof(*str) * x + 1);
	if (!str)
		return (NULL);
	while (s[y] != '\0')
	{
		str[y] = (*f)(y, s[y]);
		y++;
	}
	str[y] = '\0';
	return (str);
}
