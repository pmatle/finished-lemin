/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 06:36:52 by pmatle            #+#    #+#             */
/*   Updated: 2017/06/11 16:52:49 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	char	*dest;
	char	*source;

	dest = (char*)dst;
	source = (char*)src;
	while (n > 0)
	{
		*dest = *source;
		if (*source == c)
		{
			source++;
			dest++;
			break ;
		}
		dest++;
		source++;
		n--;
	}
	if (n == 0)
		return (NULL);
	return (dest);
}
