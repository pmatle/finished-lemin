/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 11:42:05 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/19 18:31:50 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int				print_map(char *str)
{
	ft_putstr(CGRN);
	ft_putendl(str);
	ft_putendl(RSET);
	free(str);
	return (1);
}

char			*get_map(char *s1, char *s2)
{
	int		len1;
	int		len2;

	len1 = ft_strlen(s1);
	len2 = ft_strlen(s2);
	s1 = (char*)realloc(s1, len1 + len2 + 2);
	s1 = ft_strcat(s1, s2);
	s1 = ft_strcat(s1, "\n");
	return (s1);
}

static t_graph	*make_graph(t_data data, int *start, int *end)
{
	t_graph		*graph;
	t_istruct	init;

	init = initialization(&graph, data.head);
	while (data.head != NULL)
	{
		if (data.head->type == 2)
			*end = data.head->index;
		if (data.head->type == 1)
			*start = data.head->index;
		data.head = data.head->next;
	}
	while (data.node != NULL)
	{
		init.list = ft_strsplit(data.node->str, '-');
		init.node1 = get_node(init.temp, init.list[0]);
		init.node2 = get_node(init.temp, init.list[1]);
		add_edge(graph, init.node1, init.node2);
		data.node = data.node->next;
		free_2d(init.list);
	}
	return (graph);
}

t_data			read_map(t_data data, int *num_ants, char **str)
{
	char		*line;
	int			tmp;

	tmp = 0;
	while (get_next_line(0, &line))
	{
		if (ft_strlen(line) == 0)
		{
			free(line);
			free(*str);
			ft_putendl(CRED"Error: File in incorrect format"RSET);
			exit(0);
		}
		if (tmp == 0)
		{
			*num_ants = ft_atoi(line);
			if (*num_ants == 0 && !ft_isdigit(line[0]))
				return (data);
			tmp++;
		}
		else
			inspect_line(&data.head, &data.node, line);
		*str = get_map(*str, line);
		free(line);
	}
	return (data);
}

int				main(void)
{
	t_data		data;
	t_start_end	se;
	int			num_ants;
	char		*str;

	data.node = NULL;
	data.graph = NULL;
	data.head = NULL;
	str = ft_strnew(1);
	num_ants = -1;
	se.start = -1;
	se.end = -1;
	data = read_map(data, &num_ants, &str);
	if (num_ants == 0)
	{
		ft_putendl_fd(CRED"Error : There are no ants exiting..."RSET, 2);
		return (0);
	}
	data.graph = make_graph(data, &se.start, &se.end);
	if (!ft_check_error(data, se, num_ants))
		return (0);
	print_map(str);
	run_algorithm(data.graph, se, num_ants);
	return (0);
}
