/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   merge_sort_list.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/29 10:43:06 by pmatle            #+#    #+#             */
/*   Updated: 2017/09/02 10:34:51 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_paths	*merge_sorted_lists(t_paths *first, t_paths *second)
{
	t_paths	*result;

	result = NULL;
	if (first == NULL)
		return (second);
	else if (second == NULL)
		return (first);
	if (ft_strlen(first->path) < ft_strlen(second->path))
	{
		result = first;
		result->next = merge_sorted_lists(first->next, second);
	}
	else
	{
		result = second;
		result->next = merge_sorted_lists(first, second->next);
	}
	return (result);
}

void	split_list(t_paths *head, t_paths **first, t_paths **second)
{
	t_paths *fast;
	t_paths *slow;

	if (head == NULL || head->next == NULL)
	{
		*first = head;
		*second = NULL;
	}
	else
	{
		slow = head;
		fast = head->next;
		while (fast != NULL)
		{
			fast = fast->next;
			if (fast != NULL)
			{
				slow = slow->next;
				fast = fast->next;
			}
		}
		*first = head;
		*second = slow->next;
		slow->next = NULL;
	}
}

void	merge_sort(t_paths **head_ref)
{
	t_paths	*head;
	t_paths	*first;
	t_paths	*second;

	head = *head_ref;
	if ((head == NULL) || (head->next == NULL))
		return ;
	split_list(head, &first, &second);
	merge_sort(&first);
	merge_sort(&second);
	*head_ref = merge_sorted_lists(first, second);
}
