/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_movement.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 09:21:42 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/19 18:31:32 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		show_movement(int ant_num, int roomid)
{
	ft_putstr(CGRN);
	ft_putchar('L');
	ft_putnbr(ant_num);
	ft_putchar('-');
	ft_putnbr(roomid);
	ft_putchar(' ');
	ft_putstr(RSET);
	return (1);
}
