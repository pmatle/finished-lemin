/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_node.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 07:07:03 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 10:55:00 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	add_node(t_infor **head, char **list, int pos)
{
	t_infor		*node;
	t_infor		*temp;
	static int	x = 0;

	node = (t_infor*)malloc(sizeof(*node));
	if (!node)
		return ;
	node->room = ft_strdup(list[0]);
	node->x = ft_atoi(list[1]);
	node->y = ft_atoi(list[2]);
	node->type = pos;
	node->index = x;
	node->next = NULL;
	if (*head == NULL)
		(*head) = node;
	else
	{
		temp = *head;
		while (temp->next != NULL)
			temp = temp->next;
		temp->next = node;
	}
	x++;
}
