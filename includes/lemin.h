/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 11:56:48 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/19 18:27:30 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H

# include <stdlib.h>
# include <stdio.h>
# include <fcntl.h>
# include "Get_next_line/get_next_line.h"
# include "libft/includes/libft.h"

# define RSET  "\x1B[0m"
# define CCYN  "\x1B[36m"
# define CRED  "\x1B[31m"
# define CGRN  "\x1B[32m"

struct			s_vertex_node
{
	int						id;
	char					*room;
	int						dist;
	char					*ant_name;
	struct s_vertex_node	*next;
}				t_vertex_node;

typedef struct	s_vertex
{
	t_vertex_node	*head;
}				t_vertex;

typedef struct	s_graph
{
	int			v;
	t_vertex	*array;
}				t_graph;

typedef struct	s_infor
{
	char			*room;
	int				index;
	int				y;
	int				x;
	int				type;
	struct s_infor	*next;
}				t_infor;

typedef struct	s_con
{
	char			*str;
	struct s_con	*next;
}				t_con;

typedef struct	s_paths
{
	char			*path;
	struct s_paths	*next;
}				t_paths;

typedef struct	s_infor_struct
{
	t_infor		*node1;
	t_infor		*node2;
	t_infor		*temp;
	char		**list;
}				t_istruct;

typedef struct	s_stack
{
	int				x;
	struct s_stack	*next;
}				t_stack;

typedef struct	s_moving
{
	int				id;
	t_stack			*stack;
	struct s_moving	*next;
}				t_moving;

typedef struct	s_arrays
{
	int		*visited;
	int		*path;
	t_graph	*graph;
}				t_arrays;

typedef struct	s_data
{
	t_infor		*head;
	t_graph		*graph;
	t_con		*node;
	t_istruct	init;
}				t_data;

typedef struct	s_start_end
{
	int		start;
	int		end;
}				t_start_end;

int				cal_distance(int x1, int y1, int x2, int y2);
void			add_edge(t_graph *graph, t_infor *src, t_infor *dest);
t_graph			*create_graph(int num_vertex);
void			print_graph(t_graph *graph);
void			find_paths(t_paths **paths, t_graph *graph, int start,
		int finish);
void			add_paths(t_paths **path, char *str);
void			print_paths(t_paths *paths);
void			merge_sort(t_paths **path);
t_paths			*remove_duplicates(t_paths *paths, t_paths *data);
void			moving(int start, int end, int num_ants, t_paths *path);
void			add_node(t_infor **head, char **list, int pos);
void			add_link(t_con **head, char *str);
void			inspect_line(t_infor **head, t_con **node, char *line);
t_infor			*get_node(t_infor *head, char *str);
int				count_nodes(t_infor *head);
t_istruct		initialization(t_graph **graph, t_infor *head);
int				run_algorithm(t_graph *graph, t_start_end se, int num_ants);
int				free_memory(t_data data);
int				show_movement(int ant_num, int roomid);
void			add_to_stack(t_stack **head, int x);
t_moving		*make_path(char *str, int start, t_stack *stack);
void			add_to_moving(t_moving **head, t_moving *node);
t_moving		*get_moving_node(int id, t_stack *stack);
int				ft_check_error(t_data data, t_start_end se, int ants);
int				free_2d(char **list);

#endif
