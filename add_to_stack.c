/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_to_stack.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 09:28:31 by pmatle            #+#    #+#             */
/*   Updated: 2017/09/07 09:31:34 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void		add_to_stack(t_stack **head, int x)
{
	t_stack		*stack;

	stack = (t_stack*)malloc(sizeof(*stack));
	stack->x = x;
	stack->next = (*head);
	(*head) = stack;
}
