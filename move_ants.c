/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_ants.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/13 17:50:45 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/19 18:29:23 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		count(t_stack *stack)
{
	int		x;

	x = 0;
	while (stack != NULL)
	{
		stack = stack->next;
		x++;
	}
	return (x);
}

int		pop(t_stack **head)
{
	t_stack		*temp;
	int			x;

	temp = (*head);
	(*head) = (*head)->next;
	x = temp->x;
	free(temp);
	return (x);
}

t_stack	*make_stack(int num_ants)
{
	t_stack		*stack;
	int			x;

	x = num_ants + 1;
	stack = NULL;
	while (x > 1)
	{
		add_to_stack(&stack, (x - 1));
		x--;
	}
	return (stack);
}

int		move_ants(t_moving **moving, int num_ants, int end)
{
	t_moving	*current;
	t_moving	*prev;
	int			len;

	current = *moving;
	while (1)
	{
		if (current->id == end)
		{
			len = count(current->stack);
			if (len == num_ants)
				return (0);
		}
		prev = current;
		current = current->next;
		len = count(current->stack);
		if (len > 0)
		{
			add_to_stack(&prev->stack, current->stack->x);
			show_movement(pop(&current->stack), prev->id);
		}
		if (current->next == NULL)
			return (1);
	}
}

void	moving(int start, int end, int num_ants, t_paths *path)
{
	t_moving	*moving;
	t_stack		*stack;
	t_moving	*temp;

	stack = make_stack(num_ants);
	moving = make_path(path->path, start, stack);
	while (1)
	{
		if (!move_ants(&moving, num_ants, end))
			return ;
		ft_putchar('\n');
	}
	while (moving != NULL)
	{
		temp = moving;
		moving = moving->next;
		free(temp);
	}
}
