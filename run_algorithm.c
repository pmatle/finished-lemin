/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_algorithm.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 08:07:42 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/19 18:28:32 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		run_algorithm(t_graph *graph, t_start_end se, int num_ants)
{
	t_paths		*paths;
	t_paths		*data;
	t_paths		*temp;

	data = NULL;
	paths = NULL;
	find_paths(&paths, graph, se.start, se.end);
	if (paths == NULL)
	{
		ft_putendl_fd("Error : No possible path to destination", 2);
		return (0);
	}
	merge_sort(&paths);
	data = remove_duplicates(paths, data);
	moving(se.start, se.end, num_ants, data);
	while (paths != NULL)
	{
		temp = paths;
		paths = paths->next;
		free(temp);
	}
	return (1);
}
