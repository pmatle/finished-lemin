/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_moving_node.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 09:40:29 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 10:15:10 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_moving	*get_moving_node(int id, t_stack *stack)
{
	t_moving	*moving;

	moving = (t_moving*)malloc(sizeof(*moving));
	moving->id = id;
	moving->stack = stack;
	moving->next = NULL;
	return (moving);
}
