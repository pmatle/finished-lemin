/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_graph.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/10 11:05:07 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 10:53:50 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_vertex_node	*create_node(int id, char *room, t_infor *h1, t_infor *h2)
{
	t_vertex_node	*node;

	node = (t_vertex_node*)malloc(sizeof(*node));
	node->id = id;
	node->room = room;
	node->dist = cal_distance(h1->x, h1->y, h2->x, h2->y);
	node->next = NULL;
	return (node);
}

t_graph			*create_graph(int vertex)
{
	t_graph	*graph;
	int		x;

	x = 0;
	graph = (t_graph*)malloc(sizeof(*graph));
	graph->v = vertex;
	graph->array = (t_vertex*)malloc(sizeof(t_vertex) * vertex);
	while (x < vertex)
	{
		graph->array[x].head = NULL;
		x++;
	}
	return (graph);
}

void			add_edge(t_graph *graph, t_infor *src, t_infor *dest)
{
	t_vertex_node	*node;

	node = create_node(dest->index, dest->room, src, dest);
	node->next = graph->array[src->index].head;
	graph->array[src->index].head = node;
	node = create_node(src->index, src->room, src, dest);
	node->next = graph->array[dest->index].head;
	graph->array[dest->index].head = node;
}

void			free_graph(t_graph *graph)
{
	int				x;
	t_vertex_node	*node;
	t_vertex_node	*temp;

	x = 0;
	while (x < graph->v)
	{
		node = graph->array[x].head;
		while (node != NULL)
		{
			temp = node;
			node = node->next;
			free(temp);
		}
		free(node);
		x++;
	}
	free(graph);
}
