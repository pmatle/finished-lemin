/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_manage.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/12 16:24:51 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 10:57:26 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void		add_paths(t_paths **paths, char *str)
{
	t_paths	*node;

	if (!(node = (t_paths*)malloc(sizeof(*node))))
		return ;
	node->path = str;
	node->next = (*paths);
	(*paths) = node;
}

void		add_paths_end(t_paths **paths, char *str)
{
	t_paths	*node;
	t_paths	*temp;

	node = (t_paths*)malloc(sizeof(*node));
	node->path = str;
	node->next = NULL;
	if (*paths == NULL)
		(*paths) = node;
	else
	{
		temp = *paths;
		while (temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = node;
	}
}

int			is_present(char *s1, char *s2)
{
	int		x;
	int		y;
	int		len1;
	int		len2;

	len1 = ft_strlen(s1);
	len2 = ft_strlen(s2);
	x = 1;
	while (s1[x] != '\0' && x < (len1 - 1))
	{
		y = 1;
		while (s2[y] != '\0' && y < (len2 - 1))
		{
			if (s1[x] == s2[y] && s1[x] != ' ')
			{
				return (1);
			}
			y++;
		}
		x++;
	}
	return (0);
}

t_paths		*check_paths(t_paths *paths, t_paths *data)
{
	t_paths	*temp;

	temp = paths;
	if (paths == NULL)
		return (NULL);
	(data == NULL) ? add_paths(&data, paths->path) : 0;
	if (paths->next == NULL)
		return (data);
	while (data != NULL)
	{
		temp = paths;
		while (temp != NULL)
		{
			if (is_present(data->path, temp->path) == 0)
			{
				add_paths(&data, temp->path);
				return (data);
			}
			temp = temp->next;
		}
		data = data->next;
	}
	return (data);
}

t_paths		*remove_duplicates(t_paths *paths, t_paths *data)
{
	data = check_paths(paths, data);
	return (data);
}
