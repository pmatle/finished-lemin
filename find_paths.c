/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_paths.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/12 12:39:17 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 11:20:12 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int			end_node(t_graph *graph, int index)
{
	t_vertex_node	*node;

	node = graph->array[index].head;
	while (node != NULL)
	{
		if (node->next == NULL)
			break ;
		node = node->next;
	}
	return (node->id);
}

int			add_path(int index, t_arrays arrays, t_paths **paths)
{
	int		x;
	char	*str;
	char	*s1;
	int		len;

	x = 0;
	str = ft_strnew(1);
	while (x < index)
	{
		s1 = ft_itoa(arrays.path[x]);
		len = ft_strlen(s1);
		str = (char*)realloc(str, sizeof(str) + len);
		str = ft_strcat(str, s1);
		if ((x + 1) != index)
		{
			str = (char*)realloc(str, sizeof(str) + 1);
			str = ft_strcat(str, " ");
		}
		x++;
	}
	add_paths(&(*paths), str);
	return (1);
}

t_paths		*save_paths(t_paths *paths, int current, int dest, t_arrays arrays)
{
	t_vertex_node	*node;
	int				end;
	static int		index = 0;

	arrays.visited[current] = 1;
	arrays.path[index] = current;
	index++;
	if (current == dest)
		add_path(index, arrays, &paths);
	else
	{
		node = arrays.graph->array[current].head;
		end = end_node(arrays.graph, current);
		while (node != NULL)
		{
			if (arrays.visited[node->id] != 1)
				paths = save_paths(paths, node->id, dest, arrays);
			node = node->next;
		}
	}
	index--;
	arrays.visited[current] = 0;
	return (paths);
}

void		find_paths(t_paths **paths, t_graph *graph, int start, int finish)
{
	t_arrays	arrays;
	int			x;
	int			index;

	index = 0;
	x = 0;
	arrays.visited = (int*)malloc(sizeof(int) * graph->v);
	arrays.path = (int*)malloc(sizeof(int) * graph->v);
	arrays.graph = graph;
	while (x < graph->v)
	{
		arrays.visited[x] = 0;
		x++;
	}
	*paths = save_paths(*paths, start, finish, arrays);
	free(arrays.visited);
	free(arrays.path);
}
