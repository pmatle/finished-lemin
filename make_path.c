/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_path.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 09:34:03 by pmatle            #+#    #+#             */
/*   Updated: 2017/11/14 10:20:22 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_moving	*make_path(char *str, int start, t_stack *stack)
{
	char		**path;
	int			x;
	t_moving	*moving;

	x = 0;
	moving = NULL;
	path = ft_strsplit(str, ' ');
	while (path[x])
	{
		if (ft_atoi(path[x]) == start)
			add_to_moving(&moving, get_moving_node(start, stack));
		else
			add_to_moving(&moving, get_moving_node(ft_atoi(path[x]), NULL));
		x++;
	}
	free_2d(path);
	return (moving);
}
